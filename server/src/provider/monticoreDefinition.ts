import { Definition, Location, Position, Range, TextDocument } from "vscode-languageserver";

const SEMICOLON_REGEX = "(?<!\\\");(?!\\\")";

export function getDefinition(textDoc: TextDocument, position: Position): Definition {
    // TODO: currently assumes that word at position always is a nonterminal
    const word = getCleanedWordAtPos(textDoc, position);
    const interfaceRegexp = getInterfaceOrExternalRegex(word);
    const interfaceDef = getRegexPositionOrNull(interfaceRegexp, textDoc);
    if (interfaceDef !== null) {
        return interfaceDef;
    }
    const ntRegexp = getFullNTRegex(word);
    return getRegexPositionOrNull(ntRegexp, textDoc);
}

function getRegexPositionOrNull(regexp: RegExp, textDoc: TextDocument): Definition {
    const matchResult = regexp.exec(textDoc.getText());
    if (matchResult != null) {
        const defRange = Range.create(textDoc.positionAt(matchResult.index),
            textDoc.positionAt(matchResult.index + matchResult.length));
        return Location.create(textDoc.uri, defRange);
    } else {
        return null;
    }
}

function getCleanedWordAtPos(textDoc: TextDocument, position: Position): string {
    const lineRange = Range.create(position.line, 0, position.line, 1000);
    const text = textDoc.getText(lineRange);
    const rawWord = getWordAt(text, position.character);
    return rawWord.replace(/.*:/, "").replace(/[^\w\s_\$]/gi, "");
}

function getWordAt(str: string, pos: number): string {
    const left = str.slice(0, pos + 1).search(/\S+$/);
    const right = str.slice(pos).search(/\s/);
    if (right < 0) {
        return str.slice(left);
    }
    return str.slice(left, right + pos);
}

function getInterfaceOrExternalRegex(nonTerminal: string): RegExp {
    return new RegExp(`(?:(?:external|interface)\\s*${nonTerminal}\\s*${SEMICOLON_REGEX})`);
}

function getFullNTRegex(nonTerminal: string): RegExp {
    return new RegExp(`(?<!\S)${nonTerminal} .*?=[\\s\\S]*?${SEMICOLON_REGEX}`);
}
