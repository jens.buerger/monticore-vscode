/**
 * Provides completions
 */

import {
    CompletionItem,
    CompletionItemKind,
    TextDocumentPositionParams,
} from "vscode-languageserver";



const simpleKeywords: string[] = ["abstract",
    "comment",
    "concept",
    "enum",
    "extends",
    "external",
    "follow",
    "fragment",
    "implements",
    "init",
    "interface",
    "max",
    "min",
    "options",
    "scope",
    "start",
    "symbol",
    "ast",
    "astrule",
    "astextends",
    "astimplements",
    "method",
    "grammar",
    "component",
];

const simpleKeywordCompletions: CompletionItem[] = simpleKeywords.map((keywordLabel) => {
    return {
        data: keywordLabel,
        kind: CompletionItemKind.Keyword,
        label: keywordLabel,
    };
});

export function getCompletions(textDocumentPosition: TextDocumentPositionParams): CompletionItem[] {
    // The pass parameter contains the position of the text document in
    // which code complete got requested. For the example we ignore this
    // info and always provide the same completion items.
    return simpleKeywordCompletions;
}


export function resolveCompletion(item: CompletionItem): CompletionItem {
    // if (item.data === GRAMMAR_COMPLETION_DATA) {
    //     item.detail = "Create a new Grammar";
    //     item.documentation = null;
    // } else if (item.data === COMPONENT_COMPLETION_DATA) {
    //     item.detail = "Make the grammar a component grammar";
    //     item.documentation = "Note that no parser is generated for component grammars";
    // }
    return item;
}


