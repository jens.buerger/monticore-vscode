/* --------------------------------------------------------------------------------------------
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 * ------------------------------------------------------------------------------------------ */

import { createConnection, ProposedFeatures, TextDocuments, TextDocumentSyncKind } from "vscode-languageserver";
import { getCompletions, resolveCompletion } from "./provider/monticoreCompletion";
import { getDefinition } from "./provider/monticoreDefinition";
import { runSafe } from "./utils";

// Creates the LSP connection
const connection = createConnection(ProposedFeatures.all);

// Create a manager for open text documents
const documents = new TextDocuments();

// The workspace folder this server is operating on
let workspaceFolder: string | null;

documents.onDidOpen((event) => {
    connection.console.log(`[Server(${process.pid}) ${workspaceFolder}] Document opened: ${event.document.uri}`);
});
documents.listen(connection);

connection.onInitialize((params) => {
    workspaceFolder = params.rootUri;
    connection.console.log(`[Server(${process.pid}) ${workspaceFolder}] Started and initialize received`);
    return {
        capabilities: {
            completionProvider: {
                resolveProvider: true,
            },
            definitionProvider: true,
            textDocumentSync: {
                change: TextDocumentSyncKind.None,
                openClose: true,
            },
        },
    };
});


// This handler provides the initial list of the completion items.
connection.onCompletion(getCompletions);

// This handler resolves additional information for the item selected in
// the completion list.
connection.onCompletionResolve(resolveCompletion);

connection.onDefinition((documentDefinitionParams, token) => {
    return runSafe(() => {
        const document = documents.get(documentDefinitionParams.textDocument.uri);
        if (document) {
            return getDefinition(document, documentDefinitionParams.position);
        }
        return null;
    }, null, `Could not find definition for ${documentDefinitionParams.textDocument.uri}`, token);
});

connection.listen();
