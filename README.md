# MontiCore VSCode

This VSCode extension provides language support for MontiCore (mc4) grammar files.

## Features

Currently the extension provides the following features:

* syntax highlighting
* syntax highlighting of inline Java code
* snippets
* go to nonterminal definition


## Known Issues

The go to definition system may be unreliable

## Release Notes

This extension is currently in beta

-----------------------------------------------------------------------------------------------------------
