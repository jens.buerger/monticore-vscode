# Development Information

* The tmLanguage syntax is generated using [Iro](https://eeyo.io/iro/)
* For the conversion of the TextMate grammar to JSON we use this [website](http://json2plist.sinaapp.com/)
* You can find an elaborate test grammar [here](https://github.com/MontiCore/monticore/blob/master/monticore-generator/it/src/main/grammars/mc/grammar/ItTestGrammar.mc4)

**IMPORTANT: After the generation and conversion of the grammar change set the javainline object as shown below to get java highlighting:**
```json
"javainline": {
    "patterns": [
        {
            "include": "source.java"
        }
    ]
},
```
